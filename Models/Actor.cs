using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TestApi.Models
{
    public class Actor
    {
        public long Id { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public DateTime LastUpdate { get; set; }
        public virtual ICollection<ActorMovie> Movies { get; set; }

    }
}