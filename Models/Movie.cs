using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestApi.Models
{
    public class Movie
    {
        public long Id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "release_year")]
        public int ReleaseYear { get; set; }
        
        public virtual ICollection<ActorMovie> Actors { get; set; }
    }
}