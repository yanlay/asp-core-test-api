using System;

namespace TestApi.Models
{
    abstract class BaseModel
    {
        protected DateTime CreatedAt { get; set; }
        
        protected DateTime UpdatedAt { get; set; } 
    }
}