using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace TestApi.Models
{
    public class ActorMovie
    {
        [Column("actor_id")]
        [JsonIgnore]
        public long ActorId { get; set; }
        public virtual Actor Actor { get; set; }
        
        [Column("film_id")]
        [JsonIgnore]
        public long MovieId { get; set; }
        public virtual Movie Movie { get; set; }
    }
}