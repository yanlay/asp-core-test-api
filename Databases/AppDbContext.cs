using System;
using Microsoft.EntityFrameworkCore;
using TestApi.Models;

namespace TestApi.Databases
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }
        
        public DbSet<Actor> Actors { get; set; }
        
        public DbSet<Movie> Movies { get; set; }
        
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<User>(user =>
            {
                user.Property(u => u.Id).HasColumnName("id");
                user.Property(u => u.Email).HasColumnName("email");
                user.Property(u => u.Password).HasColumnName("password");
                user.Property(u => u.Token).HasColumnName("token");
            });
            
            modelBuilder.Entity<User>().ToTable("users");
            
            modelBuilder.Entity<Actor>(actor =>
            {
                actor.Property(a => a.Id).HasColumnName("id");
                actor.Property(a => a.FirstName).HasColumnName("first_name");
                actor.Property(a => a.LastName).HasColumnName("last_name");
                actor.Property(a => a.LastUpdate).HasColumnName("last_update").HasDefaultValueSql("getutcdate()");
            });
            
            modelBuilder.Entity<Actor>().ToTable("actors");
            
            
            modelBuilder.Entity<Movie>(movie =>
            {
                movie.Property(m => m.Id).HasColumnName("id");
                movie.Property(m => m.Title).HasColumnName("title");
                movie.Property(m => m.Description).HasColumnName("description");
                movie.Property(m => m.ReleaseYear).HasColumnName("release_year");
            });
            modelBuilder.Entity<Movie>().ToTable("films");
            
            modelBuilder.Entity<ActorMovie>()
                .HasKey(am => new { am.ActorId, am.MovieId });

            modelBuilder.Entity<ActorMovie>()
                .ToTable("film_actor");
        }
    }
}