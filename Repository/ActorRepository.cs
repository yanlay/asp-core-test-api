using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApi.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestApi.Models;
using TestApi.Databases;

namespace TestApi.Repository 
{
    
    public class ActorRepository : Repository<Actor>, IActorRepository
    {
        public ActorRepository(AppDbContext context) : base(context)
        {

        }

        public IEnumerable<Actor> GetAllWithMovies()
        {
            return _context.Actors.Include(a => a.Movies);
        }
        public async Task<Actor> GetWithMovies(long id)
        {
            return await _context.Actors
                .Where(a => a.Id == id)
                .Include(a => a.Movies)
                .FirstOrDefaultAsync();
        }
    }
}