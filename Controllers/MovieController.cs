using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestApi.Databases;
using TestApi.Models;

namespace TestApi.Controllers
{
    [ApiController]
    [Route("api/movies")]
    public class MovieController : ControllerBase
    {
        private readonly AppDbContext _context;
        
        public MovieController(AppDbContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Movie>>> GetAllFilm()
        {
            return await _context.Movies.ToListAsync();
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetById(long id)
        {
            var movie = await _context.Movies
                .Where(a => a.Id == id)
                .Include(a => a.Actors)
                .ThenInclude(ml => ml.Actor)
                .FirstAsync();

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        [HttpPost]
        public async Task<ActionResult<Movie>> SaveMovie([Bind("Title,Description,ReleaseYear")]
            Movie movie)
        { 
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetById), new {id = movie.Id}, movie);
        }
    }
}