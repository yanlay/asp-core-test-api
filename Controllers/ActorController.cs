using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestApi.Repository;
using TestApi.Interfaces;
using TestApi.Models;

namespace TestApi.Controllers
{
    [ApiController]
    [Route("api/actors")]
    public class ActorController : ControllerBase
    {

        private readonly IActorRepository _actorRepository;

        public ActorController(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        [HttpGet]
        public async Task<ActionResult> GetAllActor()
        {
            var actors =  await _actorRepository.GetAll();

            return Ok(actors);
        }
        

        [HttpPost]
        public async Task<ActionResult> SaveActor([Bind("FirstName,LastName")]Actor actor)
        {
            actor.LastUpdate = DateTime.Now;

            await _actorRepository.Create(actor);

            return CreatedAtAction(nameof(GetById), new {id = actor.Id}, actor);
        }
        

        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(long id)
        {
            var actor = await _actorRepository.GetById(id);
            
            if (actor == null)
            {
                return NotFound();
            }

            return Ok(actor);
            
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateActor(long id, Actor actor)
        {
            var existingActor = await _actorRepository.GetById(id);

            if ( existingActor == null)
            {
                return NotFound();
            }
            existingActor.FirstName = actor.FirstName;
            existingActor.LastName = actor.LastName;
            existingActor.LastUpdate = DateTime.Now;

            await _actorRepository.Update(existingActor);

            return CreatedAtAction(nameof(GetById), new {id = actor.Id}, existingActor);
        }

        [HttpDelete("{id}")]

        public async
    }
}