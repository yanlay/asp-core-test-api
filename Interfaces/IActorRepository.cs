using TestApi.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TestApi.Interfaces
{
    public interface IActorRepository : IRepository<Actor>
    {
        IEnumerable<Actor> GetAllWithMovies();

        Task<Actor> GetWithMovies(long id);
    }
}