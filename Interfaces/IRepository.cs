using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace TestApi.Interfaces
{

    public interface IRepository<T> where T :class {
        Task<List<T>> GetAll();

        IEnumerable<T> Find(Func<T, bool> predicate);

        Task<T> GetById(long id);

        Task<int> Create(T entity);

        Task<int> Update(T entity);

        Task<int> Delete(T entity);

        int Count(Func<T, bool> predicate);

    }

}